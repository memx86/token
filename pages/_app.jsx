import { Provider } from "react-redux";

import { wrapper } from "../redux/store";

import Layout from "../components/Layout";
import "../styles/index.scss";

const App = ({ Component, ...rest }) => {
  const { store, props } = wrapper.useWrappedStore(rest);
  return (
    <Provider store={store}>
      <Layout>
        <Component {...props.pageProps} />
      </Layout>
    </Provider>
  );
};

export default App;
