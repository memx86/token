import { useSelector } from "react-redux";

import { wrapper } from "../redux/store";
import { getBpi, getBpiThunk } from "../redux/bpi";
import Token from "../components/Token";
import { useSetReqInterval } from "../hooks";

export const getServerSideProps = wrapper.getServerSideProps(
  (store) => async () => {
    await store.dispatch(getBpiThunk());
    return {
      props: {},
    };
  }
);

const Bpi = () => {
  const bpi = useSelector(getBpi);
  useSetReqInterval(getBpiThunk);

  return (
    <>
      <Token token={bpi} />
    </>
  );
};

export default Bpi;
