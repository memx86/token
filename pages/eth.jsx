import { useSelector } from "react-redux";

import { wrapper } from "../redux/store";
import { getEth, getEthThunk } from "../redux/eth";
import Token from "../components/Token";
import { useSetReqInterval } from "../hooks";

export const getServerSideProps = wrapper.getServerSideProps(
  (store) => async () => {
    await store.dispatch(getEthThunk());
    return {
      props: {},
    };
  }
);

const Eth = () => {
  const eth = useSelector(getEth);
  useSetReqInterval(getEthThunk);

  return (
    <>
      <Token token={eth} />
    </>
  );
};

export default Eth;
