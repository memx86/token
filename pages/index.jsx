import Link from "next/link";
import { useSelector } from "react-redux";
import IntervalForm from "../components/IntervalForm";
import { getIntervalValue } from "../redux/interval";

import s from "../styles/pages/Home.module.scss";

export default function Home() {
  const interval = useSelector(getIntervalValue);
  return (
    <>
      <h1 className={s.title}>Welcome to crypto price check app</h1>
      <p className={s.subtitle}>
        To check a price of a desired currency just click a link below
      </p>
      <ul className={s.list}>
        <li>
          <Link href="bpi">
            <a className={s.link}>Bitcoin</a>
          </Link>
        </li>
        <li>
          <Link href="eth">
            <a className={s.link}>Ethereum</a>
          </Link>
        </li>
      </ul>
      <p className={s.text}>
        Prices update every {interval} seconds, you can change it below
      </p>
      <IntervalForm />
    </>
  );
}
