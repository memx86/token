import { configureStore } from "@reduxjs/toolkit";
import { createWrapper } from "next-redux-wrapper";
import { bpiSlice } from "./bpi";
import { ethSlice } from "./eth";
import { intervalSlice } from "./interval";

const makeStore = () =>
  configureStore({
    reducer: {
      [bpiSlice.name]: bpiSlice.reducer,
      [ethSlice.name]: ethSlice.reducer,
      [intervalSlice.name]: intervalSlice.reducer,
    },
  });

export const wrapper = createWrapper(makeStore);
