import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

export const intervalSlice = createSlice({
  name: "interval",
  initialState: {
    value: 60,
  },
  reducers: {
    setIntervalState: (state, { payload }) => ({
      ...state,
      value: payload,
    }),
  },
  extraReducers: {
    [HYDRATE]: (state) => ({
      ...state,
    }),
  },
});

export const { setIntervalState } = intervalSlice.actions;
