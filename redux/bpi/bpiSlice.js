import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { getBpiThunk } from "./bpiThunks";
import { initialTokenState } from "../../constants";

export const bpiSlice = createSlice({
  name: "bpi",
  initialState: initialTokenState,
  extraReducers: {
    [HYDRATE]: (state, { payload }) => ({
      ...state,
      ...payload.bpi,
    }),
    [getBpiThunk.fulfilled]: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
  },
});
