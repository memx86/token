import { createAsyncThunk } from "@reduxjs/toolkit";
import { initialTokenState } from "../../constants";
import { fetchBpi } from "../../services";

export const getBpiThunk = createAsyncThunk(
  "bpi/get",
  async (_, { rejectWithValue }) => {
    try {
      const bpi = await fetchBpi();
      const dto = { ...initialTokenState };

      Object.keys(dto).forEach((key) => {
        const rate = bpi[key].rate_float;
        const preparedRate = Number(rate.toFixed(2));
        dto[key] = preparedRate;
      });
      return dto;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
