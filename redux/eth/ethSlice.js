import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { getEthThunk } from "./ethThunks";
import { initialTokenState } from "../../constants";

export const ethSlice = createSlice({
  name: "eth",
  initialState: initialTokenState,
  extraReducers: {
    [HYDRATE]: (state, { payload }) => ({
      ...state,
      ...payload.eth,
    }),
    [getEthThunk.fulfilled]: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
  },
});
