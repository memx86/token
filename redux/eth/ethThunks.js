import { createAsyncThunk } from "@reduxjs/toolkit";
import { fetchEth } from "../../services";

export const getEthThunk = createAsyncThunk(
  "eth/get",
  async (_, { rejectWithValue }) => {
    try {
      const data = await fetchEth();
      return data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
