import { useEffect, useState } from "react";

export const useValidation = (value, rules = []) => {
  const [isValid, setIsValid] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    const hasPassed = rules.every((rule) => {
      const { hasPassed, errorMessage } = rule(value);

      if (!hasPassed) setErrorMessage(errorMessage);

      return hasPassed;
    });

    setIsValid(hasPassed);
  }, [value, rules]);

  return { isValid, errorMessage };
};
