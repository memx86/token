import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getIntervalValue } from "../redux/interval";

export const useSetReqInterval = (thunk) => {
  const dispatch = useDispatch();
  const interval = useSelector(getIntervalValue);

  useEffect(() => {
    const intervalId = setInterval(() => {
      dispatch(thunk());
    }, interval * 1000);
    return () => {
      clearInterval(intervalId);
    };
  }, [dispatch, interval, thunk]);
};
