import Container from "./Container";
import NavBar from "./NavBar";

import s from "../styles/components/Header.module.scss";

const Header = () => {
  return (
    <header className={s.header}>
      <Container>
        <NavBar />
      </Container>
    </header>
  );
};

export default Header;
