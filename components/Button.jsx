import PropTypes from "prop-types";
import s from "../styles/components/Button.module.scss";

const Button = ({ className = "", children, ...props }) => {
  return (
    <button className={`${s.btn} ${className}`} {...props}>
      {children}
    </button>
  );
};

Button.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

export default Button;
