import { useRouter } from "next/router";
import PropTypes from "prop-types";
import Link from "next/link";

import s from "../styles/components/ActiveLink.module.scss";

const ActiveLink = ({ href, text }) => {
  const router = useRouter();
  const path = router.pathname;
  const isActive = path === href;

  return (
    <Link href={href}>
      <a className={`${s.link} ${isActive ? s.active : ""}`}>{text}</a>
    </Link>
  );
};

ActiveLink.propTypes = {
  href: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default ActiveLink;
