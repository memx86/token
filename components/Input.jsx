import PropTypes from "prop-types";
import { useEffect } from "react";

import { useValidation } from "../hooks/useValidation";
import s from "../styles/components/Input.module.scss";

const Input = ({
  value,
  setValue,
  isTouched,
  setIsTouched,
  formSetValid,
  rules,
  label,
  className = "",
  ...props
}) => {
  const { isValid, errorMessage } = useValidation(value, rules);

  useEffect(() => {
    formSetValid(isValid);
  }, [formSetValid, isValid]);

  const inputClass = isValid ? s.inputSuccess : s.inputError;

  const onChange = (e) => {
    const value = e.target.value;
    if (!isTouched) setIsTouched(true);

    setValue(value);
  };

  const hasValue = !!value;
  return (
    <label className={s.container}>
      <input
        {...props}
        value={value}
        onChange={onChange}
        className={`${s.input} ${className} ${isTouched ? inputClass : ""}`}
      />
      <span className={`${s.label} ${hasValue ? s.labelUp : ""}`}>{label}</span>
      {!isValid && <span className={s.error}>{errorMessage}</span>}
    </label>
  );
};

Input.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  setValue: PropTypes.func.isRequired,
  isTouched: PropTypes.bool.isRequired,
  setIsTouched: PropTypes.func.isRequired,
  formSetValid: PropTypes.func.isRequired,
  rules: PropTypes.arrayOf(PropTypes.func),
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default Input;
