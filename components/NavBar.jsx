import ActiveLink from "./ActiveLink";
import s from "../styles/components/Navbar.module.scss";

const NavBar = () => {
  return (
    <nav className={s.nav}>
      <ul className={s.list}>
        <li>
          <ActiveLink href="/" text="Home" />
        </li>
        <li>
          <ActiveLink href="/bpi" text="Bitcoin" />
        </li>
        <li>
          <ActiveLink href="/eth" text="Ethereum" />
        </li>
      </ul>
    </nav>
  );
};

export default NavBar;
