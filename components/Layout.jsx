import PropTypes from "prop-types";
import Head from "next/head";

import Header from "./Header";
import Footer from "./Footer";
import Container from "./Container";

import s from "../styles/components/Layout.module.scss";

const Layout = ({ children }) => {
  return (
    <div className={s.wrapper}>
      <Head>
        <title>Crypto prices</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <main>
        <Container className={s.container}>{children}</Container>
      </main>
      <Footer />
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
