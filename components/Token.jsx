import { useMemo } from "react";
import PropTypes from "prop-types";

import s from "../styles/components/Token.module.scss";

const Token = ({ token }) => {
  const preparedTokens = useMemo(
    () =>
      Object.keys(token).map((key) => ({
        currency: key,
        rate: token[key],
      })),
    [token]
  );

  return (
    <ul className={s.list}>
      {preparedTokens.map(({ currency, rate }) => (
        <li key={currency}>
          {currency} : {rate}
        </li>
      ))}
    </ul>
  );
};

Token.propTypes = {
  token: PropTypes.shape({
    USD: PropTypes.number.isRequired,
    GBP: PropTypes.number.isRequired,
    EUR: PropTypes.number.isRequired,
  }).isRequired,
};

export default Token;
