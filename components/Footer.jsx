import Container from "./Container";

import s from "../styles/components/Footer.module.scss";

const Footer = () => {
  return (
    <footer className={s.footer}>
      <Container>
        <p className={s.text}>&copy; Yevhen Malyshko, 2022</p>
      </Container>
    </footer>
  );
};

export default Footer;
