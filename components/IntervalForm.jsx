import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getIntervalValue, setIntervalState } from "../redux/interval";

import { required, positiveInt, min } from "../validation";

import Button from "./Button";
import Input from "./Input";

import s from "../styles/components/IntervalForm.module.scss";

const rules = [
  required(),
  positiveInt(),
  min(1, "Don't set to less than 1 second please"),
];

const IntervalForm = () => {
  const intervalValue = useSelector(getIntervalValue);
  const [time, setTime] = useState(intervalValue);
  const [isValid, setisValid] = useState(true);
  const [isTouched, setIsTouched] = useState(false);
  const dispatch = useDispatch();

  const onSubmit = (e) => {
    e.preventDefault();
    dispatch(setIntervalState(Number(time)));
    setIsTouched(false);
    document.activeElement.blur();
  };

  return (
    <form className={s.form} onSubmit={onSubmit}>
      <Input
        type="text"
        value={time}
        setValue={setTime}
        isTouched={isTouched}
        setIsTouched={setIsTouched}
        label="seconds"
        rules={rules}
        formSetValid={setisValid}
        placeholder=" "
      />
      <Button type="submit" disabled={!isValid}>
        Ok
      </Button>
    </form>
  );
};

export default IntervalForm;
