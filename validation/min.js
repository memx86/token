export const min =
  (minValue, message = `min value is ${minValue}`) =>
  (value) => {
    const hasPassed = value >= minValue;
    return {
      hasPassed,
      errorMessage: hasPassed ? "" : message,
    };
  };
