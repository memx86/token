export const positiveInt =
  (message = "Not a positive integer") =>
  (value) => {
    const regex = /^[0-9]+$/;
    const hasPassed = regex.test(value);
    return {
      hasPassed,
      errorMessage: hasPassed ? "" : message,
    };
  };
