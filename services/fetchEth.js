import axios from "axios";

export const fetchEth = async () => {
  const { data } = await axios.get(
    "https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD,EUR,GBP"
  );
  return data;
};
