import axios from "axios";

export const fetchBpi = async () => {
  const { data } = await axios.get(
    "https://api.coindesk.com/v1/bpi/currentprice.json"
  );
  return data.bpi;
};
